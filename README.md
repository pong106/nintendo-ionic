Ionic 2 App Base
=====================

This is the base template for Ionic 2 starter apps.

## Release
For unsafe Https image to work, manually change 


```
#!

project/platforms/android/CordovaLib/src/org/apache/cordova/engine/SystemWebViewClient.java
```
Method: onReceivedSslError - [[Source]](http://ivancevich.me/articles/ignoring-invalid-ssl-certificates-on-cordova-android-ios/)
```
#!java

// THIS IS WHAT YOU NEED TO CHANGE:
      // 1. COMMENT THIS LINE
      // super.onReceivedSslError(view, handler, error);
      // 2. ADD THESE TWO LINES
      // ---->
      handler.proceed();
      return;
```



## Using this project

You'll need the Ionic CLI with support for v2 apps:

```bash
$ npm install -g ionic
```

Then run:

```bash
$ ionic start myApp
```

More info on this can be found on the Ionic [Getting Started](http://ionicframework.com/docs/v2/getting-started/) page.