import { Component } from '@angular/core';
import {Http} from '@angular/http';
import { NavController, Platform,NavParams } from 'ionic-angular';
import { InAppBrowser } from 'ionic-native';

@Component({
  selector: 'page-publishers',
  templateUrl: 'publishers.html'
})
export class PublishersPage {
  pubs:any = [];
  url:any = 'https://www.google.com/search?q=';
  constructor(public navCtrl: NavController, public navParams: NavParams, private http:Http,public platform: Platform) {

          var url = "";
          if (this.platform.is('android')) {
            url = "/android_asset/www";
          }

  this.http.get(url + '/assets/publishers.json').map(res => res.json()).subscribe(data => {
    for (let key in data) {
      if(!data[key].name.US)
      {
        //get first of the object
        this.pubs.push({key:key,
          value: data[key].name[Object.keys(data[key].name)[0]],
          url:  this.url + data[key].name[Object.keys(data[key].name)[0]]
          });
      }
      else {
        this.pubs.push({key:key,
          value: data[key].name.US,
          url: this.url + data[key].name.US
        });
      }
    }
  });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PublishersPage');
  }
  itemTapped(event, item) {
    let browser = new InAppBrowser(item.url, "_system");
  }

}
