import { Component } from '@angular/core';

import { NavController, NavParams, Platform } from 'ionic-angular';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {
  item: any;

  constructor(public navCtrl: NavController,public navParams: NavParams) {
    this.item = navParams.get('item');
  }

}
