import { Component,ViewChild } from '@angular/core';
import {Http} from '@angular/http';
import { NavController, NavParams, Platform,LoadingController,Content,Searchbar } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Page1 } from '../page1/page1';

@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html'
})
export class Page2 {
  @ViewChild(Searchbar) searchBar: Searchbar;
  @ViewChild(Content) content: Content;
  selectedItem: any;
  icons: any;
  posts: any;
  wup: any = [];
  ctr: any = [];
  oposts: any;
  items: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private http:Http, public platform: Platform, private loadingCtrl: LoadingController) {
      //init segment
      this.icons = "both";

      // Show the loading message
      let loadingPopup = this.loadingCtrl.create({
        content: 'Loading games...'
      });
      loadingPopup.present();

      var url = "";
      if (this.platform.is('android')) {
        url = "/android_asset/www";
      }

      let keys = [];
      this.http.get(url + '/assets/complete-us.json').map(res => res.json()).subscribe(data => {
        for (let key in data) {
          if(data[key].name){
            keys.push({key: key, value: data[key]});
            if(data[key].platform_device == 'WUP')
            {
              this.wup.push({key: key, value: data[key]});
            }
            else if(data[key].platform_device == 'CTR')
            {
              this.ctr.push({key: key, value: data[key]});
            }
          }
        }

        this.oposts = keys;
        this.posts = keys;

        // Hide the loading message
        loadingPopup.dismiss();
      });
    }

    onInput(ev: any) {
      // set val to the value of the searchbar
      let val = ev.target.value;

      if(  this.icons.indexOf('both') > -1)
      {
        this.posts = this.oposts;
      }
      else if(  this.icons.indexOf('WUP') > -1)
      {
        this.posts = this.wup;
      }
      else
      {
        this.posts = this.ctr;
      }

      // if the value is an empty string don't filter the items
      if (val && val.trim() != '' && val.length > 2) {
        this.posts = this.posts.filter((item) => {
          if(item.value.name)
          {
            return (item.value.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
          }
          return false;
        });
      }
        this.content.scrollToTop()
    }
    filter(val)
    {
      if(val.indexOf('both') > -1)
      {
        // this.icons = 'both';
        this.posts = this.oposts;
      }
      else if(val.indexOf('WUP') > -1)
      {
        // this.icons = 'wup';
        this.posts = this.wup;
      }
      else
      {
        // this.icons = '3ds';
        this.posts = this.ctr;
      }
      // this.searchBar='';
      this.searchBar.value='';
      this.content.scrollToTop();
      return;
    }
    virtualTrack(index, item) {
      return item.id;
    }
    selectedTabChanged($event): void {
      this.filter(this.icons);
    }
    itemTapped(event, item) {
      this.navCtrl.push(Page1, {
        item: item
      });
    }
  }
