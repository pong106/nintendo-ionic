import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {bootstrap} from 'angular2/platform/browser';
import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
